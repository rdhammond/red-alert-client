import React from 'react';
import { StyleSheet, Text, View, Button, Linking } from 'react-native';
import RedAlertApi from './RedAlertApi';

export default class ContactInfoScreen extends React.Component {
	constructor() {
		super();
		this.state = {
			contactInfo: {
				name: '',
				email: '',
				phone: '',
				address1: '',
				address2: '',
				city: '',
				state: '',
				zip: ''
			},
			error: false,
			loading: true,
			timerId: null
		};
		this.api = new RedAlertApi();
	}

	async componentWillMount() {
		await this.loadContactInfo();
	}

	async loadContactInfo() {
		const timerId = this.state.timerId;
		if (timerId) clearTimeout(timerId);
		this.setState({loading: true, timerId: null});

		try {
			let contactInfo = await this.api.getContactInfo();
			if (!contactInfo) {
				throw new Error('Contact info not provided.');
			}
			this.setState({contactInfo, loading: false, error: false});
		}
		catch(e) {
			const loadContactInfo = this.loadContactInfo.bind(this);
			this.setState({loading: false, error: true, timerId: setTimeout(loadContactInfo, 15*60*1000)});
		}
	}

	componentWillUmount() {
		const timerId = this.state.timerId;
		if (timerId) clearTimeout(timerId);
	}

	cityStateZip(info) {
		if (!info) return '';
		var sb = [];

		if (info.city) {
			sb.push(info.city);
			if (info.state) sb.push(', ');
		}
		if (info.state) sb.push(info.state);
		if (info.zipcode) {
			sb.push(' ');
			sb.push(info.zipcode);
		}
		return sb.join('');
	}

	phone(info) {
		return !info || !/^[0-9]{10}$/.test(info.phone)
			? ''
			: `(${info.phone.substr(0,3)}) ${info.phone.substr(3,3)}-${info.phone.substr(6,4)}`;
	}

	openCall(info) {
		if (info && info.phone)
			Linking.openURL(`tel:${info.phone}`);
	}

	openText(info) {
		if (info && info.phone)
			Linking.openURL(`sms:${info.phone}`);
	}

	openEmail(info) {
		if (info && info.email)
			Linking.openURL(`mailto:${encodeURI(info.email)}`);
	}

	loadingView() {
		return (
			<View style={styles.messageContainer}>
				<Text style={styles.loading}>Loading...</Text>
			</View>
		);
	}

	errorView() {
		return (
			<View style={styles.messageContainer}>
				<Text style={styles.errorHeader}>Oops!</Text>
				<Text style={styles.errorBody}>We lost contact with the server. Please try again in a little while.</Text>
			</View>
		);
	}

	render() {
		const { contactInfo, error, loading } = this.state;

		if (loading)
			return this.loadingView();

		if (error)
			return this.errorView();

		const { name, email, address1, address2 } = contactInfo;
		const phone = this.phone(contactInfo);
		const cityStateZip = this.cityStateZip(contactInfo);
		const openCall = this.openCall.bind(this, contactInfo);
		const openEmail = this.openEmail.bind(this, contactInfo);
		const openText = this.openText.bind(this, contactInfo);

		return (
			<View style={styles.container}>
				<Text style={styles.name}>{name}</Text>
				<Text style={styles.text}>{email}</Text>
				<Text style={styles.text}>{phone}</Text>
				<Text style={styles.text}>{address1}</Text>
				<Text style={styles.text}>{address2}</Text>
				<Text style={styles.text}>{cityStateZip}</Text>
				<Button onPress={openCall} title="Call" color="#841584" />
				<Button onPress={openText} title="Text" color="#841584" />
				<Button onPress={openEmail} title="Email" style={styles.button} color="#841584" />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20
	},
	name: {
		fontSize: 24,
		fontWeight: 'bold'
	},
	text: {
		fontSize: 18
	},
	messageContainer: {
		flex: 1,
		justifyContent: 'center',
		alignContent: 'center',
		padding: 10
	},
	loading: {
		fontSize: 18,
		fontStyle: 'italic'
	},
	errorHeader: {
		fontSize: 24,
		fontWeight: 'bold',
		marginBottom: 20
	},
	errorBody: {
		fontSize: 16
	}
});
