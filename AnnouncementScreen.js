import moment from 'moment';
import React from 'react';
import { NavigationActions } from 'react-navigation';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

export default class AnnouncementScreen extends React.Component {
	static navigationOptions = {
		title: 'Announcements'
	};

	render() {
		const { navigation } = this.props;
		const item = navigation.getParam('item');
		const { title, body } = item;
		const sentOn = new Date(item.sentOn);
		const date = moment(sentOn).format('M/D/YYYY');
		const time = moment(sentOn).format('h:mm A');

		return (
			<ScrollView contentContainerStyle={styles.view}>
				<Text style={styles.title}>{title}</Text>
				<Text style={styles.datetime}>{date} @ {time}</Text>
				<Text style={styles.body}>{body}</Text>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	view: {
		padding: 10
	},
	title: {
		fontSize: 24
	},
	datetime: {
		marginBottom: 15
	},
	body: {
		fontSize: 16
	}
});
