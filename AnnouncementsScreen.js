import moment from 'moment';
import React from 'react';
import { TouchableOpacity, FlatList, StyleSheet, Text, View, RefreshControl } from 'react-native';
import RedAlertApi from './RedAlertApi';
import { Notifications } from 'expo';

class ListItem extends React.Component {
	render() {
		const { item, isFirst, onPress } = this.props;
		const { title, body } = item;
		const sentOn = new Date(item.sentOn);
		const date = moment(sentOn).format('M/D/YYYY');
		const time = moment(sentOn).format('h:mm A');

		return (
			<TouchableOpacity onPress={onPress}>
				<View style={[styles.listItem, {borderTopWidth: isFirst ? 1 : 0}]}>
					<View style={styles.header}>
						<Text style={styles.title} numberOfLines={1} ellipsizeMode='tail'>{title}</Text>
						<View style={styles.datetime}>
							<Text>{date}</Text>
							<Text>{time}</Text>
						</View>
					</View>
					<Text style={styles.body} numberOfLines={3} ellipsizeMode='tail'>{body}</Text>
				</View>
			</TouchableOpacity>
		);
	}
}

export default class AnnouncementsScreen extends React.Component {
	constructor() {
		super();
		this.state = {
			refreshing: false,
			announcements: [],
			timerId: null
		};
		this.api = new RedAlertApi();
	}

	onPress(item) {
		this.props.navigation.navigate('Announcement', {item});
	}

	async loadAnnouncements() {
		const timerId = this.state.timerId;
		if (timerId) clearTimeout(timerId);
		this.setState({refreshing: true, timerId: null});

		try {
			let announcements = await this.api.getAnnouncements();
			this.setState({announcements});
		}
		catch(e) {
			alert('Error while contacting server. Updates may be down for a bit.');
		}
		this.setState({refreshing: false});

		const loadAnnouncements = this.loadAnnouncements.bind(this);
		this.setState({timerId: setTimeout(loadAnnouncements, 15*60*1000)});
	}

	async componentWillMount() {
		await this.loadAnnouncements();
	}

	handleNotifications(notification) {
		if (!notification.data || notification.data.type !== 'announcement')
			return;

		this.loadAnnouncements();
		if (notification.origin !== 'selected')
			return;

		this.props.navigation.navigate('Announcements');
	}

	componentDidMount() {
		this._listener = Notifications.addListener(this.handleNotifications.bind(this));
	}

	componentWillUnmount() {
		const timerId = this.state.timerId;
		if (timerId) clearTimeout(timerId);
	}

	renderListItem(item) {
		const onPress = this.onPress.bind(this, item);

		return (
			<ListItem item={item} onPress={onPress} />
		);
	}

	render() {
		const refreshing = this.state.refreshing;
		const loadAnnouncements = this.loadAnnouncements.bind(this);
		const renderListItem = this.renderListItem.bind(this);

		const refreshControl = (
			<RefreshControl refreshing={refreshing} onRefresh={loadAnnouncements} />
		);

		return (
			<FlatList
				data={this.state.announcements}
				renderItem={({item, index}) => renderListItem(item)}
				keyExtractor={x => x._id}
				refreshControl={refreshControl}
			/>
		);
	}
}

const styles = StyleSheet.create({
	listItem: {
		flex: 0,
		backgroundColor: '#fff',
		padding: 10,
		borderBottomWidth: 1
	},
	header: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginBottom: 10
	},
	title: {
		fontSize: 24,
		fontWeight: 'bold',
		width: '75%'
	},
	address: {
		fontSize: 18
	},
	datetime: {
	}
});
