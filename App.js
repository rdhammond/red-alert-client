import React from 'react';
import { Platform, AsyncStorage } from 'react-native';
import { createStackNavigator, createBottomTabNavigator, withNavigation } from 'react-navigation';
import { AppLoading, Permissions, Notifications } from 'expo';
import EventsScreen from './EventsScreen';
import EventScreen from './EventScreen';
import AnnouncementsScreen from './AnnouncementsScreen';
import AnnouncementScreen from './AnnouncementScreen';
import ContactInfoScreen from './ContactInfoScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Api from './RedAlertApi';

console.ignoredYellowBox = ['Setting a timer'];


export default class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {isReady: false};
	}

	async registerForPushNotificationAsync() {
		try {
			let pushRefused = await AsyncStorage.getItem('@RedAlert:pushRefused');
			if (pushRefused) return;

			let {status} = await Permissions.getAsync(Permissions.NOTIFICATIONS);
			if (status !== 'granted')
				status = (await Permissions.askAsync(Permissions.NOTIFICATIONS)).status;
			if (status !== 'granted') {
				await AsyncStorage.setItem('@RedAlert:pushRefused', true);
				return;
			}

			let token = await Notifications.getExpoPushTokenAsync();
			let api = new Api();
			await api.sendPushToken(token);
		}
		catch (err) {
			console.warn(err);
		}

		this.setState({isReady: true});
	}

	createTabNavigator() {
		const {notification} = (this.props.exp || {});
		const openedFromNotification = notification && (Platform.OS === 'android' || notification.origin === 'selected');

		let initialRouteName = 'Announcements';

		if (openedFromNotification && notification.data && notification.data.type === 'event') {
			initialRouteName = 'Events';
		}

		return createBottomTabNavigator(
			{
				Announcements: AnnouncementsScreen,
				Events: EventsScreen,
				ContactInfo: ContactInfoScreen
			},
			{
				initialRouteName,
				navigationOptions: ({ navigation }) => ({
					tabBarIcon: ({ focused, tintColor }) => {
						const { routeName } = navigation.state;
						var iconName;
						switch (routeName) {
							case 'Announcements':
								iconName = 'ios-chatbubbles';
								break;
							case 'Events':
								iconName = 'ios-compass';
								break;
							case 'ContactInfo':
								iconName = 'ios-mail';
								break;
						}
						return <Ionicons name={iconName} size={25} color={tintColor} />;
					}
				}),
				tabBarOptions: {
					activeTintColor: '#4075ba',
					inactiveTintColor: '#888888'
				}
			}
		);
	}

	createRootStack() {
		return createStackNavigator(
			{
				Home: this.createTabNavigator(),
				Announcement: AnnouncementScreen,
				Event: EventScreen
			},
			{
				initialRouteName: 'Home',
				navigationOptions: {
					title: 'Kerri for Senate',
					headerBackTitle: 'Back',
					headerTruncatedBackTitle: 'Back',
					headerStyle: {
						backgroundColor: '#4075ba',
					},
					headerTintColor: '#fff',
					headerTitleStyle: {
						fontWeight: 'bold',
					}
				}
			}
		);
	}

	render() {
		const registerForPushNotificationsAsync = this.registerForPushNotificationAsync.bind(this);

		if (!this.state.isReady)
			return (
				<AppLoading
					startAsync={registerForPushNotificationsAsync}
					onFinish={() => this.setState({isReady: true})}
					onError={console.warn}
				/>
			);

		const RootStack = this.createRootStack();
		return <RootStack />
	}
}
