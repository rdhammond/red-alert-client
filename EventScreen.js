import moment from 'moment';
import React from 'react';
import { NavigationActions } from 'react-navigation';
import { Platform, StyleSheet, Text, View, Button, Linking, ScrollView } from 'react-native';
import { Permissions, Location, MapView } from 'expo';
import RedAlertApi from './RedAlertApi';

const { Marker, Callout } = MapView;

const deltas = {
	latitudeDelta: 0.03507,
	longitudeDelta: 0.044075
};

export default class EventScreen extends React.Component {
	static navigationOptions = {
		title: 'Events'
	};

	onPress(item) {
		const addr = encodeURI(item.address);

		if (Platform.OS === 'ios')
			Linking.openURL(`https://maps.apple.com/maps?daddr=${addr}`);
		else
			Linking.openURL(`https://maps.google.com?saddr=Current+Location&daddr=${addr}`);
	}

	render() {
		const { navigation } = this.props;
		const item = navigation.getParam('item');
		const { title, address, description, distance, loc } = item;
		const eventTime = new Date(item.eventTime);
		const lat = loc.coordinates[1];
		const lon = loc.coordinates[0];
		const region = {latitude: lat, longitude: lon, ...deltas};
		const date = moment(eventTime).format('M/D/YYYY');
		const time = moment(eventTime).format('h:mm A');
		const onPress = this.onPress.bind(this, item);

		return (
			<View style={styles.view}>
				<ScrollView>
					<View style={styles.header}>
						<Text style={styles.title}>{title}</Text>
						<Text>{distance}</Text>
					</View>
					<Text style={styles.address}>{address}</Text>
					<Text style={styles.datetime}>{date} @ {time}</Text>
					<Text style={styles.description}>{description}</Text>
				</ScrollView>
				<Button onPress={onPress} title="Get Directions" color="#4075ba" />
				<MapView initialRegion={region} onPress={onPress} style={styles.map}>
					<Marker coordinate={{latitude: lat, longitude: lon}} />
				</MapView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	view: {
		padding: 10,
		flex: 1
	},
	body: {
		height: '60%',
		flexShrink: 1,
		flex: 1
	},
	header: {
		flexDirection: 'row'
	},
	map: {
		height: '40%'
	},
	title: {
		flexGrow: 1,
		fontSize: 24
	},
	datetime: {
		marginBottom: 15
	},
	description: {
		fontSize: 18
	}
});
