const baseUrl = 'http://192.168.1.5/api';
const apiKey = 'FILL_ME_IN';

export default class RedAlertApi {
	getUrl(action) {
		return `${baseUrl}/${action}?apikey=${apiKey}`;
	}

	getAnnouncements() {
		return fetch(this.getUrl('announcements'))
			.then(x => x.json())
			.then(x => x.announcements);
	}

	getEvents() {
		return fetch(this.getUrl('events'))
			.then(x => x.json())
			.then(x => x.events);
	}

	getContactInfo() {
		return fetch(this.getUrl('contactinfo'))
			.then(x => x.json());
	}

	sendPushToken(token) {
		return fetch(this.getUrl('pushtokens'), {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({expoToken: token})
		})
		.then(x => x.json());
	}
}
