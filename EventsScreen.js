import moment from 'moment';
import React from 'react';
import { TouchableOpacity, FlatList, StyleSheet, Text, View, RefreshControl } from 'react-native';
import RedAlertApi from './RedAlertApi';
import { Notifications } from 'expo';

class ListItem extends React.Component {
	render() {
		const { item, isFirst, onPress } = this.props;
		const { title, address, description } = item;
		const eventTime = new Date(item.eventTime);
		const firstAddr = (address || '').split(',')[0].trim();
		const date = moment(eventTime).format('M/D/YYYY');
		const time = moment(eventTime).format('h:mm A');

		return (
			<TouchableOpacity onPress={onPress}>
				<View style={[styles.listItem, {borderTopWidth: isFirst ? 1 : 0}]}>
					<View style={styles.header}>
						<Text style={styles.title} numberOfLines={1} ellipsizeMode='tail'>{title}</Text>
					</View>
					<Text style={styles.address}>{address}</Text>
					<Text style={styles.datetime}>{date} @ {time}</Text>
				</View>
			</TouchableOpacity>
		);
	}
}

export default class EventsScreen extends React.Component {
	static navigationOptions = {
		title: 'Events'
	};

	constructor() {
		super();
		this.state = {
			refreshing: false,
			events: [],
			timerId: null
		};
		this.api = new RedAlertApi();
	}

	onPress(item) {
		this.props.navigation.navigate('Event', {item});
	}

	async componentWillMount() {
		await this.loadEvents();
	}

	handleNotification(notification) {
		if (!notification.data || notification.data.type !== 'event')
			return;

		this.loadEvents();
		if (notification.origin !== 'selected')
			return;

		this.props.navigation.navigate('Events');
	}

	componentDidMount() {
		this._listener = Notifications.addListener(this.handleNotification.bind(this));
	}

	componentWillUmount() {
		const timerId = this.state.timerId;
		if (timerId) clearTimeout(timerId);
		if (this._listener) Notifications.removeListener(this._listener);
	}

	async loadEvents() {
		const timerId = this.state.timerId;
		if (timerId) clearTimeout(timerId);
		this.setState({refreshing: true, timerId: null});

		try {
			const events = await this.api.getEvents();
			this.setState({events});
		}
		catch(e) {
			alert('Error while contacting server. Updates may be down for a bit.');
		}
		this.setState({refreshing: false});

		const loadEvents = this.loadEvents.bind(this);
		this.setState({timerId: setTimeout(loadEvents, 15*60*1000)});
	}

	renderListItem(item) {
		const onPress = this.onPress.bind(this, item);

		return (
			<ListItem item={item} onPress={onPress} />
		);
	}

	render() {
		const loadEvents = this.loadEvents.bind(this);
		const renderListItem = this.renderListItem.bind(this);

		const refreshControl = (
			<RefreshControl refreshing={this.state.refreshing} onRefresh={loadEvents} />
		);

		return (
			<FlatList
				data={this.state.events}
				renderItem={({item, index}) => renderListItem(item)}
				keyExtractor={x => x._id}
				refreshControl={refreshControl}
			/>
		);
	}
}

const styles = StyleSheet.create({
	listItem: {
		flex: 0,
		backgroundColor: '#fff',
		padding: 10,
		borderBottomWidth: 1
	},
	header: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	title: {
		fontSize: 24,
		fontWeight: 'bold'
	},
	datetime: {
		marginTop: 10,
		fontSize: 18
	}
});
